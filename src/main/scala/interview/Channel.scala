package interview

import akka.actor.Actor
import akka.pattern.ask

import com.github.nscala_time.time.Imports._
import scala.concurrent.{duration, Await}

case class Channel(uuid: Int, name: String) {

  /**
   * Compare equality
   * @param in Compare with this channel
   * @return Boolean
   */
  def ==(in: Channel) = in.uuid == this.uuid

  /**
   * Alternative of above equality operator check
   * @param in Compare with this channel
   * @return Boolean
   */
  def equals(in: Channel) = this == in

  /**
   * Compare two channels with their last used time
   * @param in Compare with this actor
   * @return Boolean
   */
  def >(in: Channel) = this.active > in.active

  /**
   * Compare two channels with their last used time
   * @param in Compare with this actor
   * @return Boolean
   */
  def <(in: Channel) = this.active < in.active


  // A database of subscribed users
  var users = collection.mutable.MutableList.empty[User]
  // Total number of messages exchanged so far
  private var totalMessages: BigInt = BigInt(0)
  // Last active timestamp
  private var active = DateTime.now
  // Flag to determine if it has a phone number
  private val hasPhone = false
  // Phone number
  var phone = false

  /**
   * Update the last active data and time to right now
   */
  def udpateActive = active = DateTime.now

  /**
   * Get last active date and time for this channel
   * @return
   */
  def getActive = active

  /**
   * Method to remove a uesr
   * @param user User to be removed
   * @return Unit
   */
  def removeUser(user: User): Channel = {
    users.dropWhile(_ == user)
    this
  }
}

case class Subscribe(channel: String, user: User, phone: Boolean)

case class Unsubscribe(channel: String, user: User)

case class BroadcastMessage(channel: String, message: Message)

case class CreateChannel(name: String)

class ChannelManager extends Actor {

  implicit val timeout = to

  // Private datastore for all the channels in the system
  private var channels = collection.mutable.MutableList.empty[Channel]
  // Private datastore for all the emptied channel UUIDs
  private var emptied = collection.mutable.MutableList.empty[Int]

  /**
   * Refresh the phone numbers associated with a channel when there is collision
   * @param channel Name of the channel a user is trying to access
   * @param user Name of the user where the collision has occurred
   * @return Boolean value as to weather the collision was removed or not
   */
  private def refreshPhone(channel: String, user: String) = {
    val users = Await.result(context.parent ? GetUsers, duration.Duration.Inf).asInstanceOf[List[User]]
    val subscriptions = users.find(_.name == user).asInstanceOf[User].subscriptions
    val available = (channels.toSet intersect subscriptions.toSet).reduce((x, y) => if(x < y) x else y)
    if(available == Nil) {
      channels = channels.map {
        c =>
          if(c.name == channel) {
            c.phone = true
            c
          } else if(c == available) {
            c.phone = false
            c
          } else c
      }
    }
  }

  /**
   * Private method to subscribe a particular user to a particular channel
   * @param name Name of the channel to subscibe to
   * @param user The user object to which the channel must be subscribed to
   * @return Boolean [as to weather the subscription was successful or not]
   */
  private def subscribe(name: String, user: User, phone: Boolean) = {
    channels = channels.map {
      channel =>
        if (channel.name == name) {
          channel.users += user
          if(user.phone && !channel.phone) this.refreshPhone(name, user.name)
          channel
        } else channel
    }
  }

  /**
   * Private method to unsubscribe a particular user to a particular channel
   * @param name Name of the channel to unsubscribe the uesr to
   * @param user The user object who must be unsubscribed
   * @return Boolean [as to weather the removal was successful or not]
   */
  private def unsubscribe(name: String, user: User) = {
    channels = channels.map(channel => if (channel.name == name) channel.removeUser(user) else channel)
  }

  /**
   * Sends a message to all the users in the channel
   * @param channel Name of the channel to send the message to
   * @param message Message that you would like to send
   * @return Unit
   */
  private def bMessage(channel: String, message: Message) = {
    channels.find(_.name == channel) match {
      case Some(c) => {
        channels = channels.map{ch =>
          if(ch == c) {
            c.udpateActive
            c
          } else c
        }
        c.users.foreach(user => context.parent ! SendMessage(user.name, message))
      }
    }
  }

  /**
   * Private method to create a channel
   * @param name Name of the channel
   * @return Unit
   */
  private def createChannel(name: String) = {
    if (channels.map(_.name) contains name) false
    else {
      if (emptied isEmpty) channels += Channel(channels.reverse.head.uuid + 1, name)
      else {
        channels += Channel(emptied.head, name)
        emptied = emptied.tail
      }
      true
    }
  }

  /**
   * Private method for removal of a channel
   * @param name Name of the channel to be removed
   * @return Boolean [weather the channel was removed or not]
   */
  private def removeChannel(name: String) = {
    channels.find(_.name == name) match {
      case Some(channel) => {
        channels.dropWhile(_ == channel)
        emptied += channel.uuid
        true
      }
      case None => false
    }
  }

  /**
   * Method to handle all the messages of this actor
   * @return Any
   */
  def receive = {
    case Subscribe(channel: String, user: User, phone: Boolean) => subscribe(channel, user, phone)
    case Unsubscribe(channel: String, user: User) => unsubscribe(channel, user)
    case BroadcastMessage(channel: String, message: Message) => bMessage(channel, message)
    case CreateChannel(name: String) => createChannel(name)
  }

}
