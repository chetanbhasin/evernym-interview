package interview

import akka.actor.Actor

case class User(uuid: Int, name: String) {
  /**
   * Comparison operator for User class
   * @param in Compare with this user
   * @return Boolean
   */
  def ==(in: User) = this.uuid == in.uuid

  /**
   * Alternative for above comparison operator
   * @param in Compare with this user
   * @return Boolean
   */
  def equals(in: User) = this == in

  /**
   * Check weather a user has any devices listed in their accounts or not
   * @return Boolean
   */
  var phone = false

  // User message inbox
  private val inbox = collection.mutable.MutableList.empty[Message]
  // Read messages
  private val readm = collection.mutable.MutableList.empty[Message]
  // User subscriptions
  var subscriptions = collection.mutable.MutableList.empty[Channel]

  /**
   * Method to add send a message to a user
   * @param message Message
   * @return Unit
   */
  def send(message: Message) = inbox += message

}

// Message classes for UserManager actor
case class CreateUser(username: String)

case class RemoveUser(username: String)

case class GetSubscriptions(username: String)

case class UserExists(username: String)

case class AddPhone(username: String)

case class SendMessage(username: String, message: Message)

case object GetUsers

class UserManager extends Actor {

  // Private collection of all the users in the system
  private var users = collection.mutable.MutableList.empty[User]

  // Private collection for emptied user indexes
  private var emptied = collection.mutable.MutableList.empty[Int]

  /**
   * Private method to create new users
   * @param username Username to be created
   * @return Boolean (weather created or not)[False when the user already exists]
   */
  private def createUser(username: String) = {
    if (users.map(_.name) contains username) false
    else {
      if (emptied isEmpty) users += User(users.reverse.head.uuid + 1, username)
      else {
        users += User(emptied.head, username)
        emptied = emptied.tail
      }
      true
    }
  }

  /**
   * Private method to remove existing users
   * @param username Username to be deleted
   * @return Boolean (weather removed or not)[true when the user didn't exist in the first place]
   */
  private def removeUser(username: String) = {
    users.find(_.name == username) match {
      case Some(user) => {
        users.dropWhile(_ == user)
        emptied += user.uuid
        true
      }
      case None => false
    }
  }

  /**
   * Method to get all the subscriptions by the user
   * @param username
   * @return List[Channel]
   */
  private def getSubscriptions(username: String) = {
    users.find(_.name == username) match {
      case Some(user) => user.subscriptions.toList
      case None => List.empty[Channel]
    }
  }

  /**
   * Private method to check if a particular user exists or not
   * @param username
   * @return Boolean
   */
  private def userExists(username: String) = {
    users.find(_.name == username) match {
      case Some(_) => true
      case None => false
    }
  }

  /**
   * Add a message to a user's message box
   * @param username Username to send the message to
   * @param message Message to be added to the user's inbox
   */
  private def sendMessage(username: String, message: Message) = {
    users = users.map {
      user =>
        if (user.name == username) user send message
        user
    }
  }

  /**
   * Private method to get all the users
   * @return List[String]: A list of all the users
   */
  private def getUsers = users.map(_.name).toList

  /**
   * Method to handle all the messages of this actor
   * @return Any
   */
  def receive = {
    case CreateUser(username) => createUser(username)
    case RemoveUser(username) => removeUser(username)
    case GetSubscriptions(username) => getSubscriptions(username)
    case UserExists(username) => userExists(username)
    case SendMessage(username: String, message: Message) => sendMessage(username, message)
    case GetUsers => getUsers
  }
}
