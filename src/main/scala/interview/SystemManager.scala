package interview

import akka.actor.{Actor, Props}
import akka.pattern.ask
import akka.util.Timeout


import scala.concurrent.Await
import scala.concurrent.duration._

/**
 * Class to manage all the messages
 * @param text Text of the message
 * @param from User from whom the message is received
 */
case class Message(channel: String, text: String, from: String)

/** * A manager for all the messages.
  * All exchange of messages must pass through this actor
  */
class SystemManager extends Actor {

  implicit val timeout = to

  // Database of all the users
  val users = context.actorOf(Props[UserManager], "UserManager")
  // Database of all the channels
  val channels = context.actorOf(Props[ChannelManager], "UserManager")

  /**
   * Receiver for all messages of this actor
   * It's used for mainly forwarding messages to appropriate actors
   * @return Any
   */
  def receive = {
    case c: CreateUser => users ! c
    case r: RemoveUser => users ! r
    case g: GetSubscriptions => sender ! Await.result(users ? g, Duration.Inf).asInstanceOf[Iterable[Channel]]
    case u: UserExists => sender ! Await.result(users ? u, Duration.Inf).asInstanceOf[Boolean]
    case m: SendMessage => users ! m
    case GetUsers => sender ! Await.result(users ? GetUsers, Duration.Inf).asInstanceOf[Iterable[User]]
    case s: Subscribe => channels ! s
    case b: BroadcastMessage => channels ! b
    case u: Unsubscribe => channels ! u
    case c: CreateChannel => channels ! c
  }

}

