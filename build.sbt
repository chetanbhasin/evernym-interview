name := """akka-scala-seed"""

version := "1.0"

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  // Change this to another test framework if you prefer
  "org.scalatest" %% "scalatest" % "2.1.6" % "test",
  // Akka
  "com.typesafe.akka" %% "akka-actor" % "2.3.5"
  //"com.typesafe.akka" %% "akka-remote" % "2.3.5",
  //"com.typesafe.akka" %% "akka-testkit" % "2.3.5"
)

// Add time library
libraryDependencies += "com.github.nscala-time" %% "nscala-time" % "2.0.0"
